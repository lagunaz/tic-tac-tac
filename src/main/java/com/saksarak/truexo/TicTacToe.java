/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksarak.truexo;

import java.util.*;

/**
 *
 * @author LagunaZi
 */
public class TicTacToe {

    static char winner = '-';
    static boolean isFinish = false;
    static Scanner kb = new Scanner(System.in);
    static int row, col, turn;
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {

        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error : this area is not empty !!!");

        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        showTable();
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        showTable();
        isFinish = true;
        winner = player;
    }

    static void checkCross() {
        if (table[0][0] == player
                && table[1][1] == player
                && table[2][2] == player) {
            showTable();
            isFinish = true;
            winner = player;
        } else if (table[2][0] == player
                && table[1][1] == player
                && table[0][2] == player) {
            showTable();
            isFinish = true;
            winner = player;
        }

    }

    static void checkDraw() {
        if (turn == 8) {
            showTable();
            isFinish = true;
            winner = '-';
        }
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkCross();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
            turn++;
        } else {
            player = 'X';
            turn++;
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " win");
        }

    }

    static void showBye() {
        System.out.println("Bye bye...");
    }

    public static void main(String[] args) {
        do {
            showWelcome();
            showTable();
            showTurn();
            input();
            checkWin();

            switchPlayer();
        } while (!isFinish);

        showResult();
        showBye();
    }

}
